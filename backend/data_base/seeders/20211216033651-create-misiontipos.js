'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const tipos = [
      {
        descripcion: 'Rutinarias',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        descripcion: 'Salvar Universo',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    return queryInterface.bulkInsert('MisionTipos', tipos, {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('MisionTipos', null, {});
  },
};
