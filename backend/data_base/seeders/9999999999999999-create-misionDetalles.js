'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const detalles = [
      {
        mision_id: 1,
        heroe_id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 1,
        heroe_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 2,
        heroe_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 2,
        heroe_id: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 3,
        heroe_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 3,
        heroe_id: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 4,
        heroe_id: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 5,
        heroe_id: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 5,
        heroe_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 6,
        heroe_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 6,
        heroe_id: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        mision_id: 6,
        heroe_id: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];

    return queryInterface.bulkInsert('MisionDetalles', detalles, {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('MisionDetalles', null, {});
  },
};
