'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const misiones = [
      {
        descripcion: 'Misión 1',
        tipo_id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        descripcion: 'Misión 2',
        tipo_id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        descripcion: 'Misión 3',
        tipo_id: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        descripcion: 'Misión 4',
        tipo_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        descripcion: 'Misión 5',
        tipo_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        descripcion: 'Misión 6',
        tipo_id: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    return queryInterface.bulkInsert('Misiones', misiones, {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Misiones', null, {});
  },
};
