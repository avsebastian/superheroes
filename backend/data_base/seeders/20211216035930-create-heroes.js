'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const heroes = [
      {
        nombre: 'Superman',
        anio_conocido: 1989,
        planeta_id: 1,
        vive: true,
        image_url:
          'https://res.cloudinary.com/dhw5qcrq7/image/upload/v1639789785/supermanProfile_r9u1yb.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Spiderman',
        anio_conocido: 1995,
        planeta_id: 2,
        vive: true,
        image_url:
          'https://res.cloudinary.com/dhw5qcrq7/image/upload/v1639789785/spidermanProfile_l1ryjp.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Chapulin',
        anio_conocido: 1992,
        planeta_id: null,
        vive: true,
        image_url:
          'https://res.cloudinary.com/dhw5qcrq7/image/upload/v1639789785/chapulinCooradoProfile_sbkqsh.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Acuaman',
        anio_conocido: 1994,
        planeta_id: null,
        vive: true,
        image_url:
          'https://res.cloudinary.com/dhw5qcrq7/image/upload/v1639789786/acuamanProfile_cwqwe8.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Batman',
        anio_conocido: 1997,
        planeta_id: null,
        vive: true,
        image_url:
          'https://res.cloudinary.com/dhw5qcrq7/image/upload/v1639789785/barmatProfile_zc7aam.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Mujer Maravilla',
        anio_conocido: 1998,
        planeta_id: 3,
        vive: false,
        image_url:
          'https://res.cloudinary.com/dhw5qcrq7/image/upload/v1639789785/mujerMaravilla_scy3ok.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Hulk',
        anio_conocido: 1999,
        planeta_id: 5,
        vive: true,
        image_url:
          'https://res.cloudinary.com/dhw5qcrq7/image/upload/v1639789785/hulkProfile_eeuhx0.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    return queryInterface.bulkInsert('Heroes', heroes, {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Heroes', null, {});
  },
};
