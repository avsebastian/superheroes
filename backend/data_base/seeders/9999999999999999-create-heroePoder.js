'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const poderes = [
      {
        heroe_id: 1,
        poder_id: 1,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 1,
        poder_id: 6,
        nivel: 2.2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 1,
        poder_id: 2,
        nivel: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 2,
        poder_id: 2,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 2,
        poder_id: 3,
        nivel: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 3,
        poder_id: 4,
        nivel: 7,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 4,
        poder_id: 3,
        nivel: 8,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 4,
        poder_id: 1,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 5,
        poder_id: 5,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 5,
        poder_id: 6,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 6,
        poder_id: 2,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 6,
        poder_id: 4,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 6,
        poder_id: 5,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        heroe_id: 7,
        poder_id: 2,
        nivel: 10,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];
    return queryInterface.bulkInsert('HeroePoderes', poderes, {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('HeroePoderes', null, {});
  },
};
