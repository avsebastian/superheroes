'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const planetas = [
      {
        nombre: 'Marte',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Venus',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Saturno',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Jupiter',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Urano',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];

    return queryInterface.bulkInsert('Planetas', planetas, {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Planetas', null, {});
  },
};
