'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const poderes = [
      {
        nombre: 'Fuerza',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Velocidad',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Teletransportación',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Telepatía',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Volar',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nombre: 'Fuego',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];

    return queryInterface.bulkInsert('Poderes', poderes, {});
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Poderes', null, {});
  },
};
