const { Mision, MisionTipo, MisionDetalle, Heroe } = require('../models/index');

module.exports = {
  async getMisiones(req, res) {
    await Mision.findAll({
      include: [
        { model: MisionTipo, as: 'Tipo', attributes: ['descripcion'] },
        {
          model: MisionDetalle,
          attributes: ['heroe_id'],
          include: [
            {
              model: Heroe,
              as: 'Heroe',
              attributes: ['id', 'nombre'],
            },
          ],
        },
      ],
      attributes: ['id', 'descripcion'],
    })
      .then((misiones) => {
        res.json({
          misiones,
        });
      })
      .catch((error) => res.json(error));
  },

  async getTipos(req, res) {
    await MisionTipo.findAll()
      .then((tipos) => res.json({ tipos }))
      .catch((error) => res.json(error));
  },

  async addMision(req, res) {
    const { description, tipo, heroes } = req.body;
    try {
      const newMision = await Mision.create({
        descripcion: description,
        tipo_id: tipo,
      });

      if (Array.isArray(heroes)) {
        heroes.map((heroe) => {
          MisionDetalle.create({
            mision_id: newMision.id,
            heroe_id: Math.floor(heroe),
          });
        });
      }

      res.status(200).json({
        msg: 'Msión agregada correctamente!',
      });
    } catch (error) {
      throw res.status(400).json({
        error,
        msgError: 'Error: Datos inválidos',
      });
    }
  },
};
