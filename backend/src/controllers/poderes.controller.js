const { Poder } = require('../models/index');

module.exports = {
  async getPoderes(req, res) {
    await Poder.findAll({
      attributes: ['id', 'nombre'],
    })
      .then((poderes) => {
        res.json({
          poderes,
        });
      })
      .catch((error) => res.json(error));
  },
};
