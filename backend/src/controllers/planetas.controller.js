const { Planeta } = require('../models/index');

module.exports = {
  async getPlanetas(req, res) {
    await Planeta.findAll({
      attributes: ['id', 'nombre'],
    })
      .then((planetas) => {
        res.json({
          planetas,
        });
      })
      .catch((error) => res.json(error));
  },
};
