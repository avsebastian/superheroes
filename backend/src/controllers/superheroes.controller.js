const { Heroe, Planeta, Poder, HeroePoder } = require('../models/index');

module.exports = {
  async getSuperheroes(req, res) {
    await Heroe.findAll({
      include: [
        { model: Planeta, as: 'planeta', attributes: ['nombre'] },
        {
          model: HeroePoder,
          as: 'poderes',
          attributes: ['nivel'],
          include: [
            { model: Poder, as: 'descripcion', attributes: ['nombre'] },
          ],
        },
      ],
      attributes: ['id', 'nombre', 'anio_conocido', 'vive', 'image_url'],
    })
      .then((heroes) => {
        res.json({
          heroes,
        });
      })
      .catch((error) => res.json(error));
  },

  addHeroe(req, res) {
    const { name, planeta, year, poderes } = req.body;

    try {
      const existHero = Heroe.findOne({ where: { name } });

      if (existHero) {
        return res
          .status(401)
          .json({ msgError: `Existe héroe registrado con el nombre ${name}` });
      }

      const heroe = Heroe.create({
        nombre: name,
        anio_conocido: year,
        planeta_id: planeta,
        vive: true, //value dafault
      });

      if (Array.isArray(poderes)) {
        poderes.map((poder) => {
          HeroePoder.create({
            heroe_id: heroe.id,
            poder_id: poder.nombre,
            nivel: poder.nivel,
          });
        });
      }

      res.status(200).json({
        msg: 'Héroe agregado correctamente!',
      });
    } catch (error) {
      throw res.status(400).json({
        error,
        msgError: 'Error: Datos inválidos',
      });
    }
  },
};
