const router = require('express').Router();
const { getPoderes } = require('../controllers/poderes.controller');

router.get('/poderes', getPoderes);

export default router;
