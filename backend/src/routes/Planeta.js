const router = require('express').Router();
const { getPlanetas } = require('../controllers/planetas.controller');

router.get('/planetas', getPlanetas);

export default router;
