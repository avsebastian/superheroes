const router = require('express').Router();
const {
  getSuperheroes,
  addHeroe,
} = require('../controllers/superheroes.controller');

router.get('/heroes', getSuperheroes);
router.post('/addHeroe', addHeroe);

export default router;
