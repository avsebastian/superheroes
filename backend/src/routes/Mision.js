const router = require('express').Router();
const {
  getMisiones,
  addMision,
  getTipos,
} = require('../controllers/misiones.controller');

router.get('/misiones', getMisiones);
router.get('/misionTipos', getTipos);
router.post('/createMision', addMision);

export default router;
