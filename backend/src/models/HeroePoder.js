'use strict';
module.exports = (sequelize, DataTypes) => {
  const HeroePoder = sequelize.define(
    'HeroePoder',
    {
      heroe_id: DataTypes.INTEGER,
      poder_id: DataTypes.INTEGER,
      nivel: DataTypes.DECIMAL,
    },
    {
      freezeTableName: true,
      tableName: 'HeroePoderes',
    }
  );

  HeroePoder.associate = (models) => {
    HeroePoder.belongsTo(models.Heroe, {
      foreignKey: 'heroe_id',
    });

    HeroePoder.belongsTo(models.Poder, {
      as: 'descripcion',
      foreignKey: 'poder_id',
    });
  };
  return HeroePoder;
};
