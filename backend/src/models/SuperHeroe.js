'use strict';
module.exports = (sequelize, DataTypes) => {
  const Heroe = sequelize.define(
    'Heroe',
    {
      nombre: {
        type: DataTypes.STRING,
        unique: true,
      },
      anio_conocido: DataTypes.INTEGER,
      planeta_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      vive: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      image_url: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'Heroes',
    }
  );

  Heroe.associate = (models) => {
    Heroe.belongsTo(models.Planeta, {
      as: 'planeta',
      foreignKey: 'planeta_id',
    });

    Heroe.hasMany(models.HeroePoder, {
      as: 'poderes',
      foreignKey: 'heroe_id',
    });

    Heroe.hasMany(models.MisionDetalle, {
      as: 'misiones',
      foreignKey: 'mision_id',
    });
  };
  return Heroe;
};
