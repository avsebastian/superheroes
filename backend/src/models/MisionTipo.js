'use strict';

module.exports = (sequelize, DataTypes) => {
  const MisionTipo = sequelize.define(
    'MisionTipo',
    {
      descripcion: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'MisionTipos',
    }
  );

  MisionTipo.associate = (models) => {
    MisionTipo.hasMany(models.Mision, { foreignKey: 'tipo_id' });
  };
  return MisionTipo;
};
