'use strict';

module.exports = (sequelize, DataTypes) => {
  const Poder = sequelize.define(
    'Poder',
    {
      nombre: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'Poderes',
    }
  );

  Poder.associate = (models) => {
    Poder.hasMany(models.HeroePoder, { foreignKey: 'poder_id' });
  };
  return Poder;
};
