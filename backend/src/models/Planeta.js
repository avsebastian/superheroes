'use strict';

module.exports = (sequelize, DataTypes) => {
  const Planeta = sequelize.define(
    'Planeta',
    {
      nombre: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      tableName: 'Planetas',
    }
  );

  Planeta.associate = (models) => {
    Planeta.hasMany(models.Heroe, { foreignKey: 'planeta_id' });
  };

  return Planeta;
};
