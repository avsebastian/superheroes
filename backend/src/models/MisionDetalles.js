'use strict';

module.exports = (sequelize, DataTypes) => {
  const MisionDetalle = sequelize.define(
    'MisionDetalle',
    {
      mision_id: DataTypes.INTEGER,
      heroe_id: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
      tableName: 'MisionDetalles',
    }
  );

  MisionDetalle.associate = (models) => {
    MisionDetalle.belongsTo(models.Mision, {
      as: 'Mision',
      foreignKey: 'mision_id',
    });
    MisionDetalle.belongsTo(models.Heroe, {
      as: 'Heroe',
      foreignKey: 'heroe_id',
    });
  };
  return MisionDetalle;
};
