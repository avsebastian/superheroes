'use strict';

module.exports = (sequelize, DataTypes) => {
  const Mision = sequelize.define(
    'Mision',
    {
      descripcion: { type: DataTypes.STRING, unique: true },
      tipo_id: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
      tableName: 'Misiones',
    }
  );

  Mision.associate = (models) => {
    Mision.belongsTo(models.MisionTipo, { as: 'Tipo', foreignKey: 'tipo_id' });
    Mision.hasMany(models.MisionDetalle, { foreignKey: 'mision_id' });
  };
  return Mision;
};
