const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv');

dotenv.config({ path: '../.env' });

const PORT = process.env.PORT || 4000;

app.use(morgan('dev'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());

app.get('/', (req, res) => {
  res.status(200).json({ message: 'API - Superhéroes' });
});
app.use(require('./routes/Planeta').default);
app.use(require('./routes/Poderes').default);
app.use(require('./routes/Superheroe').default);
app.use(require('./routes/Mision').default);

app.listen(PORT, () => {
  console.log(`Servidor corriendo en puerto ${PORT}`);
});

export default app;
