import Image from 'next/image';
import imgHeroes from '../public/heroes.png';

const Main = () => {
  return (
    <div className="container mx-auto px-6 sm:px-12 flex flex-col sm:flex-row items-center">
      <div className="w-1/2 xl:w-2/6 flex flex-col items-start sm:py-0">
        <h1 className="text-6xl xl:text-10xl font-abhaya-libre text-blue-800 font-bold leading-none">
          Bienvenido
        </h1>
        <h2 className="text-xl xl:text-3xl font-abhaya-libre text-blue-400 uppercase font-bold leading-none tracking-widest -mt-2 mb-6">
          Landing page
        </h2>
      </div>
      <div className="bg-contain bg-center sm:flex items-center justify-center xl:justify-end w-1/2 xl:w-4/6">
        <Image src={imgHeroes} alt="Picture of the author" placeholder="blur" />
      </div>
    </div>
  );
};

export default Main;
