import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';

import { store } from '../../store';
const { useModelDispatchers, useModelState } = store;

const FormAddHeroe = () => {
  const { getPlanetas, getPoderes } = useModelDispatchers('app');
  const { planetas, poderes } = useModelState('app');
  const { addHeroe } = useModelDispatchers('heroes');

  const router = useRouter();

  useEffect(() => {
    getPoderes();
    getPlanetas();
  }, []);

  const initialValues = {
    name: '',
    planeta: null,
    year: '',
    poderes: [{ nombre: null, nivel: '' }],
  };

  const validationSchema = yup.object({
    name: yup.string().required('Campo obligatorio'),
    year: yup.string().required('Campo obligartorio'),
    poderes: yup.array(
      yup.object({
        nivel: yup.string().required('Campo obligatirio'),
      })
    ),
  });

  const {
    values,
    handleSubmit,
    errors,
    handleChange,
    setFieldValue,
    getFieldProps,
  } = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values) => {
      console.log(values);
      addHeroe(values)
        .then((res) =>
          Swal.fire({
            showConfirmButton: true,
            //icon: 'success',
            text: `${res}`,
          })
        )
        .then(() => router.push('/heroes'))
        .catch(
          (error) => console.log('entro aca error::', error)
          // Swal.fire({
          //   showConfirmButton: true,
          //   icon: 'error',
          //   text: `${error}`,
          // })
        );
    },
  });

  const handleNewField = () => {
    setFieldValue('poderes', [...values.poderes, { nombre: null, nivel: '' }]);
  };

  const handleRemoveField = (id) => {
    setFieldValue(
      'poderes',
      ...values.poderes.filter((poder) => poder.id !== id)
    );
  };

  if (!planetas || !poderes) {
    return <div>Cargando...</div>;
  }

  return (
    <div className="bg-gray-100 py-10 px-auto ">
      <div className="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto">
        <form onSubmit={handleSubmit}>
          <div className="flex items-center mb-5">
            <label
              htmlFor="name"
              className="inline-block w-20 mr-6 text-right 
                                 font-bold text-gray-600"
            >
              Nombre
            </label>
            <input
              type="text"
              name="name"
              placeholder="Nombre"
              value={values.name}
              onChange={handleChange}
              className="flex-1 py-2 border-b-2 border-gray-400 focus:border-green-400 
                      text-gray-600 placeholder-gray-400
                      outline-none"
            />
            {errors.name && (
              <p className="mt-2 text-xs italic text-red-500">{errors.name}</p>
            )}
          </div>
          <div className="flex items-center mb-5">
            <label
              htmlFor="number"
              className="inline-block w-20 mr-6 text-right 
                                 font-bold text-gray-600"
            >
              Planeta
            </label>
            <select
              name="planeta"
              className="flex-1 py-2 border-b-2 border-gray-400 focus:border-green-400 
                      text-gray-600 placeholder-gray-400
                      outline-none"
              onChange={handleChange}
            >
              <option value="">Tierra</option>
              {planetas.map((planeta) => (
                <option value={planeta.id}>{planeta.nombre}</option>
              ))}
            </select>
          </div>
          <div className="flex items-center mb-5">
            <label
              htmlFor="number"
              className="inline-block w-20 mr-6 text-right 
                                 font-bold text-gray-600"
            >
              Año Descubierto
            </label>
            <input
              type="number"
              name="year"
              placeholder="aaaa"
              min="1"
              max="2022"
              step="1"
              value={values.year}
              onChange={handleChange}
              className="flex-1 py-2 border-b-2 border-gray-400 focus:border-green-400 
                      text-gray-600 placeholder-gray-400
                      outline-none"
            />
            {errors.year && (
              <p className="mt-2 text-xs italic text-red-500">{errors.year}</p>
            )}
          </div>

          {values.poderes.map((poder, index) => (
            <div key={index} className="flex items-center mb-5">
              <label
                htmlFor="name"
                className="inline-block w-20 mr-6 text-right 
                                 font-bold text-gray-600"
              >
                Poder
              </label>
              <select
                className="flex-1 py-2 border-b-2 border-gray-400 focus:border-green-400 
                      text-gray-600 placeholder-gray-400
                      outline-none"
                {...getFieldProps(`poderes[${index}].nombre`)}
              >
                <option value={null}>Elegír poder</option>
                {poderes.map((power) => (
                  <option value={power.id}>{power.nombre}</option>
                ))}
              </select>
              <label
                htmlFor="number"
                className="inline-block w-20 mr-6 text-right 
                                 font-bold text-gray-600"
              >
                Nivel
              </label>
              <input
                type="number"
                name="nivel"
                placeholder="nivel"
                min="0.1"
                max="9999"
                step="0.1"
                className="flex-1 py-2 border-b-2 border-gray-400 focus:border-green-400 
                      text-gray-600 placeholder-gray-400
                      outline-none"
                {...getFieldProps(`poderes[${index}.nivel]`)}
              />
              {/* {errors.poderes && (
                <p className="mt-2 text-xs italic text-red-500">
                  {errors.poderes[index].nivel}
                </p>
              )} */}
            </div>
          ))}
          <div className="text-center my-3">
            <button
              type="button"
              className="py-3 px-8 bg-green-400 hover:bg-green-500 text-white font-bold"
              onClick={handleNewField}
            >
              Agregar Poder
            </button>
          </div>
          <div className="text-center my-3">
            <button
              className="py-3 px-8 bg-blue-400 hover:bg-blue-500 text-white font-bold"
              type="submit"
            >
              Añadir Heroe
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default FormAddHeroe;
