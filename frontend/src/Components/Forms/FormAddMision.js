import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useFormik } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';

import { store } from '../../store';
const { useModelDispatchers, useModelState } = store;

const FormAddHeroe = () => {
  const { tipoMision } = useModelState('app');
  const { getMisionTipos } = useModelDispatchers('app');
  const { heroes } = useModelState('heroes');
  const { createMision } = useModelDispatchers('misiones');

  useEffect(() => {
    getMisionTipos();
  }, []);

  const router = useRouter();

  const initialValues = {
    description: '',
    tipo: '',
    heroes: [],
  };

  const validationSchema = yup.object({
    description: yup.string().required('Campo obligatorio'),
    tipo: yup.string().required('Campo obligartorio'),
    heroes: yup.array().required().min(1, 'Seleccione al menos un héroe'),
  });

  const { values, handleSubmit, errors, handleChange, setFieldValue } =
    useFormik({
      initialValues,
      validationSchema,
      onSubmit: (values) => {
        createMision(values)
          .then((res) =>
            Swal.fire({
              showConfirmButton: true,
              icon: 'success',
              text: `${res}`,
            })
          )
          .then(() => router.push('/misiones'));
      },
    });

  const handleChanges = (e) => {
    const { checked, name } = e.target;
    if (checked) {
      setFieldValue('heroes', [...values.heroes, name]);
    } else {
      setFieldValue(
        'heroes',
        values.heroes.filter((v) => v !== name)
      );
    }
  };

  if (!heroes || !tipoMision) {
    return <div>Cargando...</div>;
  }

  return (
    <div className="bg-gray-100 py-10 px-auto ">
      <div className="bg-white p-10 md:w-3/4 lg:w-1/2 mx-auto">
        <form onSubmit={handleSubmit}>
          <div className="flex items-center mb-5">
            <label
              htmlFor="name"
              className="inline-block w-20 mr-6 text-right 
                                 font-bold text-gray-600"
            >
              Descripción
            </label>
            <input
              type="text"
              name="description"
              placeholder="Descripción"
              value={values.description}
              onChange={handleChange}
              className="flex-1 py-2 border-b-2 border-gray-400 focus:border-green-400 
                      text-gray-600 placeholder-gray-400
                      outline-none"
            />
            {errors.description && (
              <p className="mt-2 text-xs italic text-red-500">
                {errors.description}
              </p>
            )}
          </div>

          <div className="flex items-center mb-5">
            <label
              htmlFor="number"
              className="inline-block w-20 mr-6 text-right 
                                 font-bold text-gray-600"
            >
              Tipo
            </label>
            <select
              name="tipo"
              className="flex-1 py-2 border-b-2 border-gray-400 focus:border-green-400 
                      text-gray-600 placeholder-gray-400
                      outline-none"
              onChange={handleChange}
            >
              <option value="">Seleccione un tipo</option>
              {tipoMision.map((tipo) => (
                <option value={tipo.id}>{tipo.descripcion}</option>
              ))}
            </select>
            {errors.tipo && (
              <p className="mt-2 text-xs italic text-red-500">{errors.tipo}</p>
            )}
          </div>

          <div className="items-center mb-5">
            <label
              htmlFor="number"
              className="w-20 mr-6 text-right 
                                 font-bold text-gray-600"
            >
              Seleccione los héroes a participar
            </label>
            <div className="grid grid-cols-2 md:grid-cols-4">
              {heroes.map((heroe, index) => (
                <div className="flex place-items-center" index={heroe.id}>
                  <div className="flex items-center py-1">
                    <input
                      className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded cursor-pointer"
                      type="checkbox"
                      id={heroe.id}
                      value={heroe.id}
                      name={heroe.id}
                      defaultChecked={values.heroes.includes(heroe)}
                      onChange={handleChanges}
                    />
                  </div>
                  <div class="ml-3 text-sm">
                    <label
                      htmlFor={heroe.id}
                      className="font-regular text-gray-700"
                    >
                      {heroe.nombre}
                    </label>
                  </div>
                </div>
              ))}
            </div>
            {errors.heroes && (
              <p className="mt-2 text-xs italic text-red-500">
                {errors.heroes}
              </p>
            )}
          </div>

          <div className="text-center my-3">
            <button
              className="py-3 px-8 bg-blue-400 hover:bg-blue-500 text-white font-bold"
              type="submit"
            >
              Crear Misión
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default FormAddHeroe;
