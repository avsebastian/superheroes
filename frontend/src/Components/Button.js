import { useRouter } from 'next/router';

const Button = ({ text, route }) => {
  const router = useRouter();

  const handleClick = () => {
    router.push(route);
  };

  return (
    <button
      className="shadow px-2 py-2 border-2 border-blue-600 rounded-full focus:outline-none focus:border-blue-600 text-blue-600 hover:bg-blue-600 hover:text-white"
      type="button"
      onClick={handleClick}
    >
      <span>{text}</span>
    </button>
  );
};

export default Button;
