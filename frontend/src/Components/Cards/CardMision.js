const CardMision = ({ mision, key }) => {
  return (
    <div key={key} className="w-full md:w-6/12 lg:w-4/12 px-4">
      <div className="bg-gradient-to-bl from-gray-600 mb-6 shadow-xl rounded-lg">
        <div className="px-6 py-6">
          <div className="text-justify">
            <h3 className="text-xl font-semibold leading-normal text-blueGray-700 mb-2">
              Misión # {mision.id}
            </h3>

            <div className="mb-2 text-blueGray-600">
              Descripción: {mision.descripcion}
            </div>
            <div className="mb-2 text-blueGray-600">
              Tipo: {mision.Tipo.descripcion}
            </div>
          </div>

          <div className="w-full text-justify ">
            <h3 className="text-xl font-semibold leading-normal text-blueGray-700">
              Heroes Participantes
            </h3>
            <div className="justify-center">
              {mision.MisionDetalles.map((heroe) => (
                <div className="text-justify flex flex-row">
                  <span className="text-base text-blueGray-400">
                    Cod.# {heroe.Heroe.id}
                  </span>
                  <span className="textbase font-bold block uppercase tracking-wide text-blueGray-600 px-2">
                    {heroe.Heroe.nombre}
                  </span>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardMision;
