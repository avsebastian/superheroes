const CardHeroe = ({ heroe, key }) => {
  return (
    <div key={key} className="w-full md:w-6/12 lg:w-4/12 px-4">
      <div className="bg-gradient-to-bl from-green-600 mb-6 shadow-xl rounded-lg">
        <div className="px-6 py-6">
          {/* <div className="justify-center">
            <div className="px-2 flex justify-center">
              <img
                className="shadow-xl rounded-full align-middle h-40 w-40 border-none -m-16 -ml-20 lg:-ml-16"
                alt="..."
                src={heroe.image_url}
              />
            </div> 
          </div> */}
          <div className="text-center">
            <h3 className="text-xl font-semibold leading-normal text-blueGray-700 mb-2">
              {heroe.nombre}
            </h3>
            <div className="text-sm leading-normal mt-0 mb-2 text-blueGray-400 font-bold uppercase">
              Planeta: {!heroe.planeta ? 'Tierra' : heroe.planeta.nombre}
              <div className="mb-2 text-blueGray-600">
                Año: {heroe.anio_conocido}
              </div>
            </div>
          </div>

          <div className="w-full text-center ">
            <h3 className="text-xl font-semibold leading-normal text-blueGray-700">
              Poderes
            </h3>
            <div className="flex justify-center py-2">
              {heroe.poderes.map((poder) => (
                <div className="px-3 text-center">
                  <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">
                    {poder.nivel}
                  </span>
                  <span className="text-sm text-blueGray-400">
                    {poder.descripcion.nombre}
                  </span>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardHeroe;
