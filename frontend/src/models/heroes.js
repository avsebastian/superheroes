import { Api } from '../services/api';

const api = Api.getInstance();

const initialState = {
  heroes: null,
  error: null,
};

const model = {
  state: initialState,
  reducers: {},
  effects: () => ({
    async getHeroes() {
      try {
        const res = await api.getHeroes();
        this.setState({
          heroes: res.data.heroes,
        });
      } catch (error) {
        this.setState({
          error: error.response.data.msg,
        });
      }
    },

    async addHeroe(formData) {
      try {
        const res = await api.createHeroe(formData);
        return res.data.msg;
      } catch (error) {
        return error;
      }
    },
  }),
};

export default model;
