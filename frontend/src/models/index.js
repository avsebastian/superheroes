import heroes from './heroes';
import misiones from './misiones';
import app from './app';

const rootModels = { heroes, misiones, app };

export default rootModels;
