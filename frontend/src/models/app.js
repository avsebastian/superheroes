import { Api } from '../services/api';

const api = Api.getInstance();

const initialState = {
  planetas: null,
  poderes: null,
  tipoMision: null,
  error: null,
};

const model = {
  state: initialState,
  reducers: {},
  effects: () => ({
    async getPlanetas() {
      try {
        const res = await api.getPlanetas();
        this.setState({
          planetas: res.data.planetas,
        });
      } catch (error) {
        this.setState({
          error: error.response.data.msg,
        });
      }
    },
    async getPoderes() {
      try {
        const res = await api.getPoderes();
        this.setState({
          poderes: res.data.poderes,
        });
      } catch (error) {
        this.setState({
          error: error.response.data.msg,
        });
      }
    },

    async getMisionTipos() {
      try {
        const res = await api.getMisionTipos();
        this.setState({
          tipoMision: res.data.tipos,
        });
      } catch (error) {
        this.setState({
          error: error.response.data.msg,
        });
      }
    },
  }),
};

export default model;
