import { Api } from '../services/api';

const api = Api.getInstance();

const initialState = {
  misiones: null,
  error: null,
};

const model = {
  state: initialState,
  reducers: {},
  effects: () => ({
    async getMisiones() {
      try {
        const res = await api.getMisiones();
        this.setState({
          misiones: res.data.misiones,
        });
      } catch (error) {
        this.setState({
          error: error.response.data.msg,
        });
      }
    },

    async createMision(formData) {
      try {
        const res = await api.createMision(formData);
        return res.data.msg;
      } catch (error) {
        return error;
      }
    },
  }),
};

export default model;
