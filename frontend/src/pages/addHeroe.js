import FormAddHeroe from '../Components/Forms/FormAddHeroe';

const addHeroe = () => {
  return (
    <section className="container m-auto bg-gray-100">
      <div className="flex justify-center items-center text-center py-3">
        <h1 className="font-bold text-3xl text-gray-900">Añadir Héroe</h1>
      </div>
      <FormAddHeroe />
    </section>
  );
};

export default addHeroe;
