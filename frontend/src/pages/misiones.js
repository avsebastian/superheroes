import { useEffect } from 'react';

import Button from '../Components/Button';
import Card from '../Components/Cards/CardMision';

import { store } from '../store';
const { useModelDispatchers, useModelState } = store;

const Misiones = () => {
  const appDispatchers = useModelDispatchers('misiones');
  const { misiones } = useModelState('misiones');

  useEffect(() => {
    appDispatchers.getMisiones();
  }, []);

  if (!misiones) {
    return <p>Cargando Misiones...</p>;
  }

  return (
    <section className="container m-auto py-5 bg-gray-50">
      <div className="flex flex-row items-center justify-between p-10 text-center">
        <h1 className="font-bold text-3xl text-gray-900">Misiones</h1>
        <Button text="Nueva Misión" route="/addMision" />
      </div>
      <div className="flex flex-row flex-wrap">
        {misiones.map((mision, index) => (
          <Card mision={mision} key={index} />
        ))}
      </div>
    </section>
  );
};

export default Misiones;
