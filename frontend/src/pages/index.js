import Main from '../Components/Main';

export default function Home() {
  return (
    <div className="container mx-auto">
      <Main />
    </div>
  );
}
