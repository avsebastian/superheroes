import React, { useEffect } from 'react';
import Head from 'next/head';

import '../styles/globals.css';
import Layout from '../Components/Layout';
import { store } from '../store';

const { Provider } = store;

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    window.location.pathname;
  }, []);

  return (
    <>
      <Head>
        <title>Super Héroes</title>
      </Head>
      <Provider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Provider>
    </>
  );
}

export default MyApp;
