import { useEffect } from 'react';

import Card from '../Components/Cards/CardHeroe';
import Button from '../Components/Button';

import { store } from '../store';
const { useModelDispatchers, useModelState } = store;

const Heroes = () => {
  const appDispatchers = useModelDispatchers('heroes');
  const { heroes } = useModelState('heroes');

  useEffect(() => {
    appDispatchers.getHeroes();
  }, []);

  if (!heroes) {
    return <p>Cargando Personajes....</p>;
  }

  return (
    <section className="container m-auto py-5 bg-gray-50">
      <div className="flex flex-row items-center justify-between p-10 text-center">
        <h1 className="font-bold text-3xl text-gray-900">Personajes</h1>
        <Button text="Agregar héroe" route="/addHeroe" />
      </div>
      <div className="flex flex-row flex-wrap">
        {heroes.map((heroe, index) => (
          <Card heroe={heroe} key={index} />
        ))}
      </div>
    </section>
  );
};

export default Heroes;
