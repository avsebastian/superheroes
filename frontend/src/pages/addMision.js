import FormAddMision from '../Components/Forms/FormAddMision';

const addHeroe = () => {
  return (
    <section className="container m-auto bg-gray-100">
      <div className="flex justify-center items-center text-center py-3">
        <h1 className="font-bold text-3xl text-gray-900">Añadir Misión</h1>
      </div>
      <FormAddMision />
    </section>
  );
};

export default addHeroe;
