import axios from 'axios';

export class Api {
  static _instance;
  url = process.env.NEXT_PUBLIC_BACKEND_URL;

  static getInstance() {
    if (!Api._instance) {
      Api._instance = new Api();
    }
    return Api._instance;
  }

  async getHeroes() {
    try {
      const res = await axios.get(`${this.url}/heroes`);
      return res;
    } catch (error) {
      console.log('getHeroes error', error);
      throw error;
    }
  }

  async getMisiones() {
    try {
      const res = await axios.get(`${this.url}/misiones`);
      return res;
    } catch (error) {
      console.log('getMisiones error', error);
      throw error;
    }
  }

  async getPlanetas() {
    try {
      const res = await axios.get(`${this.url}/planetas`);
      return res;
    } catch (error) {
      console.log('getPlanetas error', error);
      throw error;
    }
  }

  async getPoderes() {
    try {
      const res = await axios.get(`${this.url}/poderes`);
      return res;
    } catch (error) {
      console.log('getPoderes error', error);
      throw error;
    }
  }

  async getMisionTipos() {
    try {
      const res = await axios.get(`${this.url}/misionTipos`);
      return res;
    } catch (error) {
      console.log('getMisionTipos error', error);
      throw error;
    }
  }

  async createHeroe(formData) {
    try {
      const res = await axios({
        method: 'post',
        url: `${this.url}/addHeroe`,
        data: formData,
      });
      return res;
    } catch (error) {
      throw error.response.data.msgError;
    }
  }

  async createMision(formData) {
    try {
      const res = await axios({
        method: 'post',
        url: `${this.url}/createMision`,
        data: formData,
      });
      return res;
    } catch (error) {
      //console.log('Errorr', error.response);
      throw error.response.data.msgError;
    }
  }
}
